unit NounsUnit;

{$mode objfpc}{$H+}

interface

{$DEFINE DebugNames}

{$DEFINE FullVersion}

uses
  Generics.Collections,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls
  {$IFDEF DebugNames}, {$IFDEF FullVersion}CastleNouns{$ELSE}CastleNounsLite{$ENDIF}{$ENDIF};

type
  TSyllables = specialize TDictionary<string, integer>;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Syllables, FirstSyllables : TSyllables;
    NameSpace: TStringList;
    BanList, Inharmonic: TStringList;
    procedure WriteUnit;
  public

  end;

var
  Form1: TForm1;
  f1: Text;
  TotalWordCount: Integer;

implementation

{$R *.lfm}

function InvertString(const aString: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to aString.Length do
    Result := Copy(aString, i, 1) + Result;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  s: string;

  procedure MakeInharmonicList;
  var
    inh: string;
  begin
    Inharmonic := TStringList.Create;
    AssignFile(f1, 'Inharmonic_CC0.txt');
    Reset(f1);
    repeat
      ReadLn(f1, inh);
      Inharmonic.Add(inh);
    until EOF(f1);
    CloseFile(f1);
  end;
  procedure MakeBanList;
  var
    swr: string;
  begin
    BanList := TStringList.Create;
    AssignFile(f1, 'SwearWords_CC0.txt');
    Reset(f1);
    repeat
      ReadLn(f1, swr);
      BanList.Add(InvertString(swr));
    until EOF(f1);
    CloseFile(f1);

    {Rewrite(f1);
    for swr in BanList do
      WriteLn(f1, swr);
    CloseFile(f1);}
  end;
  function GetChar(Index: integer): string;
  begin
    Result := Copy(s, Index, 1);
  end;
  procedure ProcessWord;
  var
    i, j, FirstVowel: Integer;
    VowelFound, FirstSyllable: boolean;
    CurrentSyllable: string;
    function isVowel(aLetter: string): boolean;
    begin
      case aLetter of
        'a','e','y','u','i','o': Result := true;
        else Result := false;
      end;
    end;
  begin
    i := 0;
    j := 1;
    FirstVowel := 1;
    VowelFound := false; //we have to process the first vowel in a special way
    FirstSyllable := true;
    repeat
      inc(i);
      if isVowel(GetChar(i)) or (i = Length(s) + 1) then
      begin
        if VowelFound then
        begin
          CurrentSyllable := Copy(s, j, i - j);
          if FirstSyllable then
          begin
            if (Length(CurrentSyllable) > 1) and (Length(CurrentSyllable) < 6)
              and not isVowel(GetChar(Length(CurrentSyllable)))
              and (i < Length(s)) then
            begin
              if FirstSyllables.ContainsKey(CurrentSyllable) then
                FirstSyllables.AddOrSetValue(CurrentSyllable, FirstSyllables.Items[CurrentSyllable] + 1)
              else
                FirstSyllables.AddOrSetValue(CurrentSyllable, 1);
            end;
            FirstSyllable := false;
            j := FirstVowel;
          end;

          CurrentSyllable := Copy(s, j, i - j);
          if (Length(CurrentSyllable) > 1) and (Length(CurrentSyllable) < 4)
            and not isVowel(GetChar(Length(CurrentSyllable)))
            then
          begin
            if Syllables.ContainsKey(CurrentSyllable) then
              Syllables.AddOrSetValue(CurrentSyllable, Syllables.Items[CurrentSyllable] + 1)
            else
              Syllables.AddOrSetValue(CurrentSyllable, 1);
          end;
          j := i;
        end else
        begin
          VowelFound := true;
          FirstVowel := i;
        end;
      end;
    until i > Length(s);
  end;
  function isEnglishLetter(const aLetter: string): boolean;
  begin
    Result :=
       (aLetter = 'q') or
       (aLetter = 'w') or
       (aLetter = 'e') or
       (aLetter = 'r') or
       (aLetter = 't') or
       (aLetter = 'y') or
       (aLetter = 'u') or
       (aLetter = 'i') or
       (aLetter = 'o') or
       (aLetter = 'p') or
       (aLetter = 'a') or
       (aLetter = 's') or
       (aLetter = 'd') or
       (aLetter = 'f') or
       (aLetter = 'g') or
       (aLetter = 'h') or
       (aLetter = 'j') or
       (aLetter = 'k') or
       (aLetter = 'l') or
       (aLetter = 'z') or
       (aLetter = 'x') or
       (aLetter = 'c') or
       (aLetter = 'v') or
       (aLetter = 'b') or
       (aLetter = 'n') or
       (aLetter = 'm');
  end;
  procedure RemoveInvalidCharacters;
  var
    i: integer;
  begin
    i := 1;
    s := LowerCase(s);
    repeat
      if not isEnglishLetter(GetChar(i)) then
        Delete(s, i, 1)
      else
        inc(i);
    until i > Length(s);
  end;
  { are the two words/syllables similar? }
  function isSimilar(A, B: string): boolean;
  begin
    //Result := A = B;
    if Length(A) = Length(B) then
      Result := A = B
    else
    if Abs(Length(A) - Length(B)) <= 1 then
      Result := (Copy(A, 1, Length(B)) = B) or (Copy(B, 1, Length(A)) = A)
    else
      Result := false;
  end;
  {}
  procedure CleanUpSyllables(var NewSyllables: TSyllables);
  var
    ss, sn: string;
    Flg: boolean;
    Count: integer;
    OldSyllables: TSyllables;
  begin
    Count := 0;
    OldSyllables := NewSyllables;
    NewSyllables := TSyllables.Create;
    for ss in OldSyllables.Keys do
    begin
      Flg := true;
      for sn in NameSpace do
        if isSimilar(sn, ss) then
        begin
          Flg := false;
          inc(Count);
          Break;
        end;
      if Flg then
        NewSyllables.Add(ss, OldSyllables.Items[ss]);
    end;
    OldSyllables.Free;
    WriteLn(Count, ' syllables removed as similar');
  end;

  procedure FilterBanned(var NewSyllables: TSyllables);
  var
    ss, Banned: string;
    Flg: boolean;
    Count: integer;
    OldSyllables: TSyllables;
  begin
    Count := 0;
    OldSyllables := NewSyllables;
    NewSyllables := TSyllables.Create;
    for ss in OldSyllables.Keys do
    begin
      Flg := true;
      for Banned in BanList do
        if Pos(Banned, ss) > 0 then
        begin
          Flg := false;
          inc(Count);
          Break;
        end;
      if Flg then
        NewSyllables.Add(ss, OldSyllables.Items[ss]);
    end;
    OldSyllables.Free;
    WriteLn(Count, ' syllables removed as banned');
  end;
  procedure FilterInharmonic(var NewSyllables: TSyllables);
  var
    ss, inh: string;
    Flg: boolean;
    Count: integer;
    OldSyllables: TSyllables;
  begin
    Count := 0;
    OldSyllables := NewSyllables;
    NewSyllables := TSyllables.Create;
    for ss in OldSyllables.Keys do
    begin
      Flg := true;
      for inh in Inharmonic do
        if Pos(inh, ss) > 0 then
        begin
          Flg := false;
          inc(Count);
          Break;
        end;
      if Flg then
        NewSyllables.Add(ss, OldSyllables.Items[ss]);
    end;
    OldSyllables.Free;
    WriteLn(Count, ' syllables removed as inharmonic');
  end;

  procedure ProcessDictionary(const aName: string; const AddToNamespace: boolean);
  begin
    AssignFile(f1, aName);
    Reset(f1);
    repeat
      inc(TotalWordCount);
      ReadLn(f1, s);
      RemoveInvalidCharacters;
      if AddToNamespace then
        NameSpace.Add(s);
      ProcessWord;
    until EOF(f1);
    CloseFile(f1);

  end;
begin
  WriteLn('Wait a bit... this can take a while...');

  MakeBanList;
  MakeInharmonicList;

  Syllables := TSyllables.Create;
  FirstSyllables := TSyllables.Create;
  NameSpace := TStringList.Create;
  NameSpace.Duplicates := dupIgnore;

  TotalWordCount := 0;
  ProcessDictionary('nounlist_cc0_by_desiquintans.txt', true);
  {$IFDEF FullVersion}
  ProcessDictionary('6of12_cc0_by_Alan_Beale.txt', true);
  ProcessDictionary('Moby_compound_forms_СС0.txt', false);
  ProcessDictionary('Moby_single_words_СС0.txt', false);
  ProcessDictionary('Scrabble_enable_СС0.txt', false);
  ProcessDictionary('Scrabble_ospd_СС0.txt', false);
  {$ENDIF}

  WriteLn('Total Word Count = ', TotalWordCount);

  FilterBanned(FirstSyllables);
  FilterBanned(Syllables);
  FilterInharmonic(FirstSyllables);
  FilterInharmonic(Syllables);
  CleanUpSyllables(FirstSyllables);
  //CleanUpSyllables(Syllables);

  WriteLn('First syllables = ', FirstSyllables.Count);
  WriteLn('Normal syllables = ', Syllables.Count);
  WriteLn;

  WriteUnit;
end;

procedure TForm1.Button1Click(Sender: TObject);
{$IFDEF DebugNames}
const
  testN = 30;
var
  i: integer;
  t: TDateTime;
{$ENDIF}
begin
  {$IFDEF DebugNames}
  t := Now;
  for i := 1 to testN do
    GenerateName;
  WriteLn('Time to generate a name = ', Round(24*60*60*1000*(Now - T) / testN), ' ms');
  for i := 1 to 30 do
    case Random(2) of
      0: Memo1.Lines.Add(GenerateName(gMale));
      1: Memo1.Lines.Add(GenerateName(gFemale));
    end;
  {$ENDIF}
end;

procedure TForm1.WriteUnit;
var
  i, N, F: Integer;
  s: String;
begin
  {$IFDEF FullVersion}
  AssignFile(f1, 'castlenouns.pas');
  {$ELSE}
  AssignFile(f1, 'castlenounslite.pas');
  {$ENDIF}
  Rewrite(f1);
  WriteLn(f1, '{ -*- buffer-read-only: t -*- }');
  WriteLn(f1, '');
  WriteLn(f1, '{ Unit automatically generated by DecoNouns,');
  {$IFDEF FullVersion}
  WriteLn(f1, '  Based on public domain word lists by Desi Quintans and Alan Beale,');
  WriteLn(f1, '  also public domain word lists by Grady Ward''s Moby project');
  WriteLn(f1, '  and Scrabble(tm) Dictionaries.');
  {$ELSE}
  WriteLn(f1, '  Based on public domain word lists by Desi Quintan.');
  {$ENDIF}
  WriteLn(f1, '  Acknowledgement of Authors is not required but highly encouraged.');
  WriteLn(f1, '');
  WriteLn(f1, '  In the current state this algorithm provides for generation');
  {$IFDEF FullVersion}
  WriteLn(f1, '  of several billions of unique names (actually up to 7e+13 names).');
  {$ELSE}
  WriteLn(f1, '  of several billions of unique names (actually up to 1e+11 names).');
  {$ENDIF}
  WriteLn(f1, '  Syllables are sorted by popularity and the generated words are');
  WriteLn(f1, '  forced to generate shorter, to provide for natural sounding names.');
  WriteLn(f1, '');
  WriteLn(f1, '  @exclude (Exclude this unit from PasDoc documentation.) }');
  {$IFDEF FullVersion}
  WriteLn(f1, 'unit CastleNouns;');
  {$ELSE}
  WriteLn(f1, 'unit CastleNounsLite;');
  {$ENDIF}
  WriteLn(f1, '');
  WriteLn(f1, 'interface');
  WriteLn(f1, '');
  WriteLn(f1, 'type');
  WriteLn(f1, '  { Character gender for which the name is generated }');
  WriteLn(f1, '  TGender = (gMale, gFemale);');
  WriteLn(f1, '');
  WriteLn(f1, '{ Generate a name, proivde a gMale or gFemale to the name');
  {$IFDEF FullVersion}
  WriteLn(f1, '  Generating a name takes about 15-20 ms on ~3MHz processor');
  {$ELSE}
  WriteLn(f1, '  Generating a name takes about 2-3 ms on ~3MHz processor');
  {$ENDIF}
  WriteLn(f1, '  Names are guaranteed to be unique }');
  WriteLn(f1, 'function GenerateName(const aGender: TGender = gMale): String;');
  WriteLn(f1, '');
  WriteLn(f1, '{ Free the memory taken by syllables and NameSpace');
  WriteLn(f1, '  Launch it when you''re done generating the names. }');
  WriteLn(f1, 'procedure FreeSyllables;');
  WriteLn(f1, '');
  WriteLn(f1, 'implementation');
  WriteLn(f1, 'uses');
  WriteLn(f1, '  Classes, SysUtils;');
  WriteLn(f1, '');
  WriteLn(f1, 'var');
  WriteLn(f1, '  FirstSyllables: array of String;');
  WriteLn(f1, '  FirstSyllablesFrequency: array of Integer;');
  WriteLn(f1, '  Syllables: array of String;');
  WriteLn(f1, '  SyllablesFrequency: array of Integer;');
  WriteLn(f1, '  BanList, InharmonicList: array of String;');
  WriteLn(f1, '  NameSpace: TStringList;');
  WriteLn(f1, '  SyllablesInitialized: Boolean = false;');
  WriteLn(f1, '');
  WriteLn(f1, 'procedure InitializeNormalSyllables;');
  WriteLn(f1, 'begin');
  i := 0;
  for s in Syllables.Keys do
  begin
    WriteLn(f1, '  Syllables[', i, '] := ''', s, ''';');
    WriteLn(f1, '  SyllablesFrequency[', i, '] := ', Syllables.Items[s], ';');
    inc(i);
  end;
  WriteLn(f1, 'end;');

  F := 0;
  i := 0;
  for s in FirstSyllables.Keys do
  begin
    if i mod 10000 = 0 then
    begin
      WriteLn(f1, '');
      WriteLn(f1, 'procedure InitializeFirstSyllables', F, ';');
      WriteLn(f1, 'begin');
    end;

    WriteLn(f1, '  FirstSyllables[', i, '] := ''', s, ''';');
    WriteLn(f1, '  FirstSyllablesFrequency[', i, '] := ', FirstSyllables.Items[s], ';');

    if (i mod 10000 = 9999) or (i = Pred(FirstSyllables.Count)) then
    begin
      WriteLn(f1, 'end;');
      inc(F);
    end;
    inc(i);
  end;

  WriteLn(f1, '');
  WriteLn(f1, 'function InvertString(const aString: string): string;');
  WriteLn(f1, 'var');
  WriteLn(f1, '  i: integer;');
  WriteLn(f1, 'begin');
  WriteLn(f1, '  Result := '''';');
  WriteLn(f1, '  for i := 1 to aString.Length do');
  WriteLn(f1, '    Result := Copy(aString, i, 1) + Result;');
  WriteLn(f1, 'end;');
  WriteLn(f1, 'procedure InitializeBanList;');
  WriteLn(f1, 'begin');
  for i := 0 to Pred(BanList.Count) do
    WriteLn(f1, '  BanList[', i, '] := InvertString(''', InvertString(BanList[i]), ''');');
  WriteLn(f1, 'end;');
  WriteLn(f1, 'procedure InitializeInharmonicList;');
  WriteLn(f1, 'begin');
  for i := 0 to Pred(Inharmonic.Count) do
    WriteLn(f1, '  InharmonicList[', i, '] := ''', Inharmonic[i], ''';');
  WriteLn(f1, 'end;');
  WriteLn(f1, '');

  //we have to split 37 thousands of records into several separate procedures
  //to avoid "procedure too complex" error :)
  i := 0;
  N := 0;
  for s in NameSpace do
  begin
    if i mod 10000 = 0 then
    begin
      WriteLn(f1, '');
      WriteLn(f1, 'procedure InitializeNameSpace', N, ';');
      WriteLn(f1, 'begin');
    end;
    WriteLn(f1, '  NameSpace.Add(''', s, ''');');
    if (i mod 10000 = 9999) or (i = Pred(NameSpace.Count)) then
    begin
      WriteLn(f1, 'end;');
      inc(N);
    end;
    inc(i);
  end;

  WriteLn(f1, '');
  WriteLn(f1, 'procedure InitializeSyllables;');
  WriteLn(f1, 'begin');
  WriteLn(f1, '  Randomize;');
  WriteLn(f1, '  NameSpace := TStringList.Create;');
  WriteLn(f1, '  NameSpace.Duplicates := dupIgnore;');
  WriteLn(f1, '  SetLength(FirstSyllables, ', FirstSyllables.Count, ');');
  WriteLn(f1, '  SetLength(FirstSyllablesFrequency, ', FirstSyllables.Count, ');');
  WriteLn(f1, '  SetLength(Syllables, ', Syllables.Count, ');');
  WriteLn(f1, '  SetLength(SyllablesFrequency, ', Syllables.Count, ');');
  WriteLn(f1, '  SetLength(BanList, ', BanList.Count, ');');
  WriteLn(f1, '  SetLength(InharmonicList, ', Inharmonic.Count, ');');
  WriteLn(f1, '  SyllablesInitialized := true;');
  for i := 0 to Pred(F) do
  WriteLn(f1, '  InitializeFirstSyllables', i, ';');
  WriteLn(f1, '  InitializeNormalSyllables;');
  WriteLn(f1, '  InitializeBanList;');
  WriteLn(f1, '  InitializeInharmonicList;');
  for i := 0 to Pred(N) do
    WriteLn(f1, '  InitializeNameSpace', i, ';');
  WriteLn(f1, 'end;');
  WriteLn(f1, '');
  WriteLn(f1, 'function GenerateName(const aGender: TGender = gMale): String;');
  WriteLn(f1, '  function GetRandomSyllable: String;');
  WriteLn(f1, '  var');
  WriteLn(f1, '    K: Integer;');
  WriteLn(f1, '  begin');
  WriteLn(f1, '    repeat');
  WriteLn(f1, '      K := Random(Length(Syllables));');
  WriteLn(f1, '    until SyllablesFrequency[K] > Random(30);');
  WriteLn(f1, '    Result := Syllables[K];');
  WriteLn(f1, '  end;');
  WriteLn(f1, '  function GetRandomFirstSyllable: String;');
  WriteLn(f1, '  var');
  WriteLn(f1, '    K: Integer;');
  WriteLn(f1, '  begin');
  WriteLn(f1, '    repeat');
  WriteLn(f1, '      K := Random(Length(FirstSyllables));');
  WriteLn(f1, '    until FirstSyllablesFrequency[K] > Random(30);');
  WriteLn(f1, '    Result := FirstSyllables[K];');
  WriteLn(f1, '  end;');
  WriteLn(f1, '  function NotBanned(aString: String): Boolean;');
  WriteLn(f1, '  var');
  WriteLn(f1, '    Banned: string;');
  WriteLn(f1, '  begin');
  WriteLn(f1, '    Result := true;');
  WriteLn(f1, '    for Banned in BanList do');
  WriteLn(f1, '      if Pos(Banned, aString) > 0 then');
  WriteLn(f1, '      begin');
  WriteLn(f1, '        Result := false;');
  WriteLn(f1, '        Exit;');
  WriteLn(f1, '      end;');
  WriteLn(f1, '  end;');
  WriteLn(f1, '  function NotInharmonic(aString: String): Boolean;');
  WriteLn(f1, '  var');
  WriteLn(f1, '    Inharm: string;');
  WriteLn(f1, '  begin');
  WriteLn(f1, '    Result := true;');
  WriteLn(f1, '    for Inharm in InharmonicList do');
  WriteLn(f1, '      if Pos(Inharm, aString) > 0 then');
  WriteLn(f1, '      begin');
  WriteLn(f1, '        Result := false;');
  WriteLn(f1, '        Exit;');
  WriteLn(f1, '      end;');
  WriteLn(f1, '  end;');
  WriteLn(f1, '  function Capitalize(aString: String): String;');
  WriteLn(f1, '  begin');
  WriteLn(f1, '    Result := UpperCase(Copy(aString, 1, 1)) + Copy(aString, 2, Length(aString));');
  WriteLn(f1, '  end;');
  WriteLn(f1, 'var');
  WriteLn(f1, '  I, W: Integer;');
  WriteLn(f1, 'begin');
  WriteLn(f1, '  if not SyllablesInitialized then');
  WriteLn(f1, '    InitializeSyllables;');
  WriteLn(f1, '  repeat');
  WriteLn(f1, '    repeat');
  WriteLn(f1, '      Result := '''';');
  WriteLn(f1, '      W := Random(2);  //using a first syllable + 1..3 syllables');
  WriteLn(f1, '      for I := 0 to W do');
  WriteLn(f1, '        Result := Result + GetRandomSyllable;');
  WriteLn(f1, '    until Length(Result) < 2.5 * (W + 1); //prevent long hard-to-say names');
  WriteLn(f1, '    Result := GetRandomFirstSyllable + Result;');
  WriteLn(f1, '  until (NameSpace.IndexOf(Result) = -1) and NotBanned(Result) and NotInharmonic(Result); //prevent duplicate names, or names that are vocabulary words');
  WriteLn(f1, '  NameSpace.Add(Result);');
  WriteLn(f1, '  if aGender = gFemale then');
  WriteLn(f1, '    Result := Result + ''a''; // add "a" ending in female names');
  WriteLn(f1, '  Result := Capitalize(Result);');
  WriteLn(f1, 'end;');
  WriteLn(f1, '');
  WriteLn(f1, 'procedure FreeSyllables;');
  WriteLn(f1, 'begin');
  WriteLn(f1, '  FirstSyllables := nil;');
  WriteLn(f1, '  FirstSyllablesFrequency := nil;');
  WriteLn(f1, '  Syllables := nil;');
  WriteLn(f1, '  SyllablesFrequency := nil;');
  WriteLn(f1, '  BanList := nil;');
  WriteLn(f1, '  InharmonicList := nil;');
  WriteLn(f1, '  FreeAndNil(NameSpace);');
  WriteLn(f1, '  SyllablesInitialized := false;');
  WriteLn(f1, 'end;');
  WriteLn(f1, '');
  WriteLn(f1, 'finalization');
  WriteLn(f1, '  FreeSyllables;');
  WriteLn(f1, 'end.');

  CloseFile(f1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FirstSyllables);
  FreeAndNil(Syllables);
  FreeAndNil(NameSpace);
  FreeAndNil(BanList);
  FreeAndNil(Inharmonic);
end;

end.

